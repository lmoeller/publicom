# SPDX-FileCopyrightText: Free Software Foundation Europe e.V.
#
# SPDX-License-Identifier: GPL-3.0-or-later

# =============================================================================
# Configure webserver
# =============================================================================

FROM bitnami/apache:2.4 as webserver-prep

COPY publicom.conf /vhosts/


# =============================================================================
# Development Preparation: run sync-docs, po4a and hugo
# =============================================================================

FROM alpine:edge as build

# Dependencies
RUN apk --no-cache add hugo


# Build hugo
COPY site/ /app/
WORKDIR /app/
RUN hugo


# =============================================================================
# Development (use the site built in dev-prep)
# =============================================================================

FROM webserver-prep as production

COPY --from=build /app/public/ /app/
